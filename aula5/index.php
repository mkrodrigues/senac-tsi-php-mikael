<?php

$alunos[0]['nome'] = 'Mikael Assis';
$alunos[0]['bitbucket'] = 'https://bitbucket.org/mkrodrigues';
$alunos[1]['nome'] = 'Mayara Manso';
$alunos[1]['bitbucket'] = 'https://bitbucket.org/myrmanso';
$alunos[2]['nome'] = 'Leandro Ramos';
$alunos[2]['bitbucket'] = 'https://bitbucket.org/leandro-silva-r';
$alunos[3]['nome'] = 'Gabrielly Vieira';
$alunos[3]['bitbucket'] = 'https://bitbucket.org/gabievs';
$alunos[4]['nome'] = 'Matheus OLiveira';
$alunos[4]['bitbucket'] = 'https://bitbucket.org/matheusOliveira';

echo '<table border="1">
        <thead>
        <th>
        Nome
        </th>
        <th>
        Bitbucket
        </th>
        </thead>';

$cor = 'white';

//Colocando cor sim cor não no for - Exemplo com operador Ternário
// for ($i = 0 ; $i < count($alunos) ; $i++){

//     $cor = $cor == 'gray' ? 'white' : 'gray';
//variavel = condicional == 'teste' ? retorna verdadeiro : retorna falso   

//     echo "<tr bgcolor='$cor'>
//             <td>{$alunos[$i]['nome']}</td>
//             <td>{$alunos[$i]['bitbucket']}</td>
//         </tr>";


// };

$cor = 'gray';
for ($i = 0; $i < count($alunos); $i++) {

    if ($cor == 'gray') {

        $cor = 'white';
    } else {
        $cor = 'gray';
    }

    echo "<tr bgcolor='$cor'>
                 <td>{$alunos[$i]['nome']}</td>
                 <td>{$alunos[$i]['bitbucket']}</td>
             </tr>";
};


echo '</table>';
echo '<br>';

// a funcao strpos retorna o índice da letra procurada no PHP
$nome = 'Mikael Assis Silva';
$posicao = strpos($nome , 'Mi');

if($posicao !== false ){
    echo "Encontrado na posição $posicao!";
}

echo '<br><br>';