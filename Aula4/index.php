<?php
//phpinfo(); //Mostra as funcões do PHP e os detalhes do ambiente
// echo '<pre>';
echo "Seu Ip é . {$_SERVER['REMOTE_ADDR']}";
// var_dump($_SERVER);

$alunos[0]['nome'] = 'Mikael Assis';
$alunos[0]['bitbucket'] = 'https://bitbucket.org/mkrodrigues';
$alunos[1]['nome'] = 'Mayara Manso';
$alunos[1]['bitbucket'] = 'https://bitbucket.org/myrmanso';
$alunos[2]['nome'] = 'Leandro Ramos';

$alunos[2]['bitbucket'] = 'https://bitbucket.org/leandro-silva-r';
$alunos[3]['nome'] = 'Gabrielly Vieira';
$alunos[3]['bitbucket'] = 'https://bitbucket.org/gabievs';
$alunos[4]['nome'] = 'Matheus OLiveira';
$alunos[4]['bitbucket'] = 'https://bitbucket.org/matheusOliveira';


echo "<pre>";
// var_dump($alunos);

// Outra forma
// $alunos1 = array(0 => array('nome' => 'Mikael Assis',
//                             'bitbucket' => 'https://'),
//                 1 => array('nome' => 'Mayara Manso',
//                             'bitbucket' => 'https://'));

// Estrutura de Repetição

// $size = count($alunos);

// $keys = array_keys($alunos);

// for($i = 0; $i < $size; $i++){
//     $key = keys[$i];
//     $value = $alunos[$key];

//     echo $value;
// }

// Resolução do Professor

echo '<table border="1">
        <thead>
        <th>
        Nome
        </th>
        <th>
        Bitbucket
        </th>
        </thead>';

for ($i = 0 ; $i < count($alunos) ; $i++){

    echo "<tr>
            <td>{$alunos[$i]['nome']}</td>
            <td>{$alunos[$i]['bitbucket']}</td>
        </tr>";

}

echo '</table>';
echo '<br>';

// FOREACH 

// foreach ($variable as $key => $value 'outro vetor') {
//     # code...
// } 

echo '<table border="1">
            <thead>
                <th>Nome</th>
                <th>Bitbucket</th>
            </thead>';

foreach ($alunos as $indice => $linha) {

    echo "<tr>
        <td>
            {$linha['nome']}
        </td>
        <td>
            {$linha['bitbucket']}
        </td>
        </tr>";    

}

echo '</table>';
echo '<br>';


// While

echo '<table border="1">
            <thead>
                <th>Nome</th>
                <th>Bitbucket</th>
            </thead>';
$i = 0;
while($i < count($alunos)){
    echo "<tr>
        <td>
            {$linha['nome']}
        </td>
        <td>
            {$linha['bitbucket']}
        </td>
        </tr>"; 
    $i++;    
}
echo '</table>';