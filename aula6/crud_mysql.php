<?php
// echo "<pre>";

// $database = mysqli_connect(
//     'localhost',
//     'root',
//     '',
//     null,
//     3306);

// var_dump($database);

if ($database = mysqli_connect(
    'localhost',
    'root',
    '',
    'aula_php',
    3306)) {
    echo "Deu certo!";

} else {
    die("Problema ao conectar ao SGBD"); //Encerra a requisição caso dê problema no SGBD
    // echo "Problema ao conectar ao Banco de Dados";
}

echo "<form method='POST'>
        Nome: <input type='text' name='nome'><br>
        Bitbucket:<input type='text' name='url'><br>
        <input type='submit' value='Enviar'>
        </form>";

$nome = isset($_POST['nome']) ? $_POST['nome'] : null;
$url = isset($_POST['url']) ? $_POST['url'] : null;

$preparada = mysqli_prepare( $database, 'INSERT INTO tb_bitbucket
                                    ( nome, url, ip)
                                    VALUES
                                    ( ?, ?, ?)');

mysqli_stmt_bind_param( $preparada, 'sss', $nome, $url, $_SERVER['REMOTE_ADDR']);

if( mysqli_stmt_execute($preparada)){
    echo "<br><br>Dados de $nome gravados no SGBD!";
};

//Fim da consulta para evitar msql injection
//Como nâo tenho dados de usuãrio (Perigoso) nâo precisp preparar a consulta SQL

$consulta = mysqli_query( $database, 'SELECT
                                        id,
                                        nome,
                                        url,
                                        ip
                                        FROM
                                        tb_bitbucket');

// echo "<pre>";

