<?php 

// Declarando a classe
// O que vier dentro da classe são atributos
// Atributo private não pode ser acessado fora da classe. Ao contrário de public que permite esse acesso *Usar private
// A função precisa ser declarada como private, senão o php assume como public
// A Classe é considerada a planta/especificação do objeto Ex.: Planta de uma casa

class Usuario {

    private $id;
    private $nome;
    private $email; 
    private $senha;
    private $objDb;

    // O Construtor é obrigatoriamente public
    public function __construct(){

        $this->objDb = new mysqli(	'localhost',
							'root',
							'',
							'aula12_php',
							3307);

        // $servidor = "localhost";
        // $usuario = "root";
        // $senha = "";
        // $SGBD = "aula12_php";
        // $conexao = mysqli_connect($servidor, $usuario, $senha, $SGBD);
    }
    

    // SETTERS
    // Um Set seta o valor para a variável ou método
    public function setId (int $id) {
        $this->id = $id;
    }

    public function setNome (string $nome) {
        $this->nome = $nome;
    }

    public function setEmail (string $email) {
        $this->email = $email;
    }

    public function setSenha (string $senha) {
        $this->senha = password_hash($senha, PASSWORD_DEFAULT);
    }

    // GETTERS
    // O Get pega o valor da classe
    public function getId (int $id) : int {
       return $this->id = $id;
    }

    public function getNome (string $nome) : string {
        return $this->nome = $nome;
    }

    public function getEmail (string $email) : string {
        return $this->email = $email;
    }

    public function getSenha (string $senha) : string {
        return $this->senha = $senha;
    }


    //Funções

    public function listarUser () : array {
        $objStmt = $this->objDb->prepare('SELECT nome, email FROM usuario WHERE id = ?');
        
        $objStmt->bind_param('i', $this->id);
        $objStmt->execute();
        $objResult = $objStmt->get_result();
        return $objResult->fetch_assoc();
    }

    public function deleteUser(){
        $objStmt = $this->objDb->prepare('DELETE FROM usuario WHERE id = ?');

        $objStmt->bind_param('i', $this->id);

        if($objStmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function saveUser(){
        $objStmt = $this->objDb->prepare('REPLACE INTO usuario (id, nome, email, senha) VALUES (?, ?, ?, ?)');
        echo $objStmt->error;
        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);
        echo $objStmt->error;
        
        if($objStmt->execute()) {
        echo $objStmt->error;
            
            return true;
        }else{
        echo $objStmt->error;

            return false;
        }
    }


    // O Destrutor destrói o objeto da classe
    // Motivos para destruir a classe: Quando chama o objeto ou quando termina de chamar o script, ele destruíra o objeto da classe automaticamente.
    public function __destruct() {
        // echo "<br> Fechando a conexão com o SGDB Teste";
    }

}


        // $objResult = $objStmt->get_result();
        // $objResult->fetch_assoc();
