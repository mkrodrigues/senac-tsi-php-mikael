<?php 

class Template {
    
    private $arquivo;
    private $variaveis;

    public function __construct (string $arquivo){
        $this->arquivo = $arquivo;
    }

    public function setVariaveis (array $vetor) {
        $this->variaveis = $vetor;
    }

    public function mostrar (){
        include($this->arquivo);
    }
}