<?php 


// Criando conexão como Banco de Dados via PDO
class Usuario {

    private $id;
    private $nome;
    private $email; 
    private $senha;
    private $objDb;

    // O Construtor é obrigatoriamente public
    public function __construct(){

        $dsn = 'mysql:dbname=aula_php;host=127.0.0.1';
        $user = 'root';
        $password = '';

    

    // SETTERS
    // Um Set seta o valor para a variável ou método
    public function setId (int $id) {
        $this->id = $id;
    }

    public function setNome (string $nome) {
        $this->nome = $nome;
    }

    public function setEmail (string $email) {
        $this->email = $email;
    }

    public function setSenha (string $senha) {
        $this->senha = password_hash($senha, PASSWORD_DEFAULT);
    }

    // GETTERS
    // O Get pega o valor da classe
    public function getId (int $id) : int {
       return $this->id = $id;
    }

    public function getNome (string $nome) : string {
        return $this->nome = $nome;
    }

    public function getEmail (string $email) : string {
        return $this->email = $email;
    }

    public function getSenha (string $senha) : string {
        return $this->senha = $senha;
    }


    //Funções




    // O Destrutor destrói o objeto da classe
    public function __destruct() {
        // echo "<br> Fechando a conexão com o SGDB Teste";
    }

}
